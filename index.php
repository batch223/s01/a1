<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Activity s01</title>
</head>
<body>

	<!-- <h1>Hello</h1> -->
	<h1>Full Address</h1>
	<p><?php echo getFullAddress('Philippines', 'Montalban', 'Rizal', '10 Mayumi st');?></p>
	<p><?php echo getFullAddress('Philippines', 'Gonzaga', 'Cagayan', '25 Mabini Rd');?></p>

	<h1>Letter-Based Grading</h1>
	<p>99 is Equivalent to <?php echo letterGrade(99) ;?></p>
	<p>96 is Equivalent to <?php echo letterGrade(96) ;?></p>
	<p>94 is Equivalent to <?php echo letterGrade(94) ;?></p>
	<p>90 is Equivalent to <?php echo letterGrade(90) ;?></p>
	<p>87 is Equivalent to <?php echo letterGrade(87) ;?></p>
	<p>84 is Equivalent to <?php echo letterGrade(84) ;?></p>
	<p>81 is Equivalent to <?php echo letterGrade(81) ;?></p>
	<p>78 is Equivalent to <?php echo letterGrade(78) ;?></p>
	<p>75 is Equivalent to <?php echo letterGrade(75) ;?></p>
	<p>70 is Equivalent to <?php echo letterGrade(70) ;?></p>
</body>
</html>